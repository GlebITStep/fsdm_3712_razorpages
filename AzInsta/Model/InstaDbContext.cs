﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AzInsta.Model
{
    public class InstaDbContext : IdentityDbContext<AppUser>
    {
        public InstaDbContext(DbContextOptions options) : base(options)
        {
        }
    }
}
