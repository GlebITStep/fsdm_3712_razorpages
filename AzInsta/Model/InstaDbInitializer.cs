﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AzInsta.Model
{
    public class InstaDbInitializer
    {
        static public async Task SeedData(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                UserManager<AppUser> userManager = serviceScope
                    .ServiceProvider
                    .GetRequiredService<UserManager<AppUser>>();

                RoleManager<IdentityRole> roleManager = serviceScope
                    .ServiceProvider
                    .GetRequiredService<RoleManager<IdentityRole>>();


                if (await roleManager.FindByNameAsync("Admin") == null)
                {
                    await roleManager.CreateAsync(new IdentityRole { Name = "Admin" });
                }

                if (await userManager.FindByNameAsync("admin@insta.az") == null)
                {
                    var user = new AppUser
                    {
                        UserName = "admin@insta.az",
                        Email = "admin@insta.az",
                        EmailConfirmed = true,
                        LockoutEnabled = false,
                        FullName = "Admin"
                    };
                    await userManager.CreateAsync(user, "Secret123$");
                    await userManager.AddToRoleAsync(user, "Admin");
                }
            }
        }
    }
}
