﻿using AzInsta.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;

namespace AzInsta.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly UserManager<AppUser> userManager;

        public AdminController(UserManager<AppUser> userManager)
        {
            this.userManager = userManager;
        }

        public async Task<IActionResult> BanUser(string username, TimeSpan time)
        {
            var user = await userManager.FindByNameAsync(username);
            user.LockoutEnd = DateTime.Now + time;
            await userManager.UpdateAsync(user);
            return Redirect("/Users");
        }

        public async Task<IActionResult> ToggleAdminRole(string username)
        {
            var user = await userManager.FindByNameAsync(username);

            if (await userManager.IsInRoleAsync(user, "Admin"))
                await userManager.RemoveFromRoleAsync(user, "Admin");
            else
                await userManager.AddToRoleAsync(user, "Admin");

            return Redirect("/Users");
        }

        public async Task<IActionResult> CanConvert(string username)
        {
            var user = await userManager.FindByNameAsync(username);
            await userManager.AddClaimAsync(user, new Claim("canConvert", "yes"));
            return Redirect("/Users");
        }
    }
}
