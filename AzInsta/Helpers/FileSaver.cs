﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AzInsta.Helpers
{
    static public class FileSaver
    {
        static public string Save(IFormFile file)
        {
            var filename = $"{Guid.NewGuid()}{file.FileName.Split('\\').Last()}";
            using (var fs = new FileStream($"wwwroot/images/{filename}", FileMode.Create))
            {
                file.CopyTo(fs);
            }
            return filename;
        }
    }
}
