﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AzInsta.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace AzInsta.Pages
{
    [Authorize(Policy = "CanConvertPolicy")]
    public class ConverterModel : PageModel
    {
        private readonly ICurrencyConverter currencyConverter;

        public ConverterModel(ICurrencyConverter currencyConverter)
        {
            this.currencyConverter = currencyConverter;
        }

        [BindProperty(SupportsGet = true)]
        public decimal AZN { get; set; }

        [BindProperty(SupportsGet = true)]
        public string Currency { get; set; }

        public decimal Result { get; set; }

        public async Task OnPostAsync()
        {
            if (AZN > 0)
            {
                decimal rate = await currencyConverter.GetRateAsync("AZN", Currency);
                Result = Math.Round(AZN / rate, 2);
            }
        }
    }
}