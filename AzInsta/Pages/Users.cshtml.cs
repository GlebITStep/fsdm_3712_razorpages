﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AzInsta.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace AzInsta.Pages
{
    [Authorize]
    public class UsersModel : PageModel
    {
        private readonly UserManager<AppUser> userManager;

        public UsersModel(UserManager<AppUser> userManager)
        {
            this.userManager = userManager;
        }

        public List<AppUser> Users { get; set; }

        public void OnGet()
        {
            Users = userManager.Users.ToList();
        }
    }
}