﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace AzInsta.Pages
{
    public class IndexModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public string Name { get; set; } = "Guest";

        [BindProperty(SupportsGet = true)]
        public int Age { get; set; } = 25;

        public void OnGet()
        {

        }

        //public void OnGet(string name = "Guest", int age = 0)
        //{
        //    Name = name;
        //    Age = age;
        //}
    }
}
