﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AzInsta.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace AzInsta.Pages
{
    [Authorize]
    public class CurrentModel : PageModel
    {
        private readonly UserManager<AppUser> userManager;

        public CurrentModel(UserManager<AppUser> userManager)
        {
            this.userManager = userManager;
        }

        public AppUser CurrentUser { get; set; }

        public async Task OnGetAsync()
        {
            CurrentUser = await userManager.GetUserAsync(HttpContext.User);
        }
    }
}