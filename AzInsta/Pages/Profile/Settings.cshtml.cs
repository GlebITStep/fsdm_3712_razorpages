﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AzInsta.Helpers;
using AzInsta.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace AzInsta.Pages.Profile
{
    public class SettingsModel : PageModel
    {
        private readonly UserManager<AppUser> userManager;

        public SettingsModel(UserManager<AppUser> userManager)
        {
            this.userManager = userManager;
        }



        [BindProperty]
        public InputModel Input { get; set; } = new InputModel();

        public async Task OnGetAsync()
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            Input.FullName = user.FullName;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                var user = await userManager.GetUserAsync(HttpContext.User);
                user.FullName = Input.FullName;

                if (Input.Image != null)
                {
                    try
                    {
                        var filename = FileSaver.Save(Input.Image);
                        user.Image = filename;
                    }
                    catch (Exception)
                    {
                        return Page();
                    }
                }

                await userManager.UpdateAsync(user);
                return RedirectToPage("/Profile/Current");
            }
            return Page();
        }

        public class InputModel
        {
            [Required]
            public string FullName { get; set; }

            public IFormFile Image { get; set; }
        }
    }
}