﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AzInsta.Services
{
    public interface ICurrencyConverter
    {
        Task<decimal> GetRateAsync(string from, string to);
    }
}
