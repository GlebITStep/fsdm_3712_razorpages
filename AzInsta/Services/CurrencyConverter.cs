﻿using AzInsta.Options;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace AzInsta.Services
{
    public class CurrencyConverter : ICurrencyConverter
    {

        CurrencyConverterOptions options;

        string api = "https://free.currconv.com/api/v7/convert";

        public CurrencyConverter(IOptions<CurrencyConverterOptions> options)
        {
            this.options = options.Value;  
        }

        public async Task<decimal> GetRateAsync(string from, string to)
        {
            HttpClient httpClient = new HttpClient();
            var jsonResult = await httpClient.GetStringAsync($@"{api}?q={to}_{from}&compact=ultra&apiKey={options.ApiKey}");
            dynamic result = JsonConvert.DeserializeObject(jsonResult);
            decimal rate = result[$"{to.ToUpper()}_{from.ToUpper()}"];
            return rate;
        }
    }
}
